(function( $ ) {
    
    $( '#size' ).on('input', function( event ) {
    	var size_val = $('#size').val();
    	$('#target').css('width', size_val+'px');
    	$('#target').css('height', size_val+'px');
      $('#target').css('line-height', size_val+'px');
    });
    $( '#button_color' ).change(function( event ) {
    	var back_color = $(this).val();
    	$('#target').css('background-color', back_color);
    	
    });
     $( '#b_radius' ).on('input', function( event ) {
    	var button_radius = $('#b_radius').val();
    	$('#target').css('border-radius', button_radius+'px');
    });
     $( '#b_text' ).on('input', function( event ) {
    	var button_text = $('#b_text').val();
    	$('#target').text(button_text);
    });
      $( '#b_t_size' ).on('input', function( event ) {
    	var b_t_size = $('#b_t_size').val();
    	$('#target').css('font-size', b_t_size+'px');
    	
    });
      $("#b_t_color").change(function(){
        var color_val = $(this).val();
        $('#target').css('color', color_val);
      });
     $( '#button_link' ).on('input', function( event ) {
    	var button_link = $('#button_link').val();
    	$('#target').attr('href', button_link);
    });//border_color
    $( '#border_color' ).change(function(){
      var border_size = $('#border_size').val();
      var border_color = $('#border_color').val();
      $('#target').css('box-shadow', '0 0 0 '+border_size+'px '+border_color);
    });
    $( '#border_size' ).on('input', function( event ) {
      var border_size = $('#border_size').val();
      var border_color = $('#border_color').val();
      $('#target').css('box-shadow', '0 0 0 '+border_size+'px '+border_color);
    });
    /*focus jquery effects*/
   
    $("#target").hover(function() {
       var size_val_f = $('#size_f').val();
        $('#target').css('width', size_val_f+'px');
        $('#target').css('height', size_val_f+'px');
        $('#target').css('line-height', size_val_f+'px');
        var back_color_f = $('#button_color_f').val();
        $('#target').css('background-color', back_color_f);
        var border_color_f = $('#border_color_f').val();
        var border_size_f = $('#border_size_f').val();
        $('#target').css('box-shadow', '0 0 0 '+border_size_f+'px '+border_color_f);
        var b_radius_f = $('#b_radius_f').val();
        $('#target').css('border-radius', b_radius_f+'px');
        var b_t_color_f = $('#b_t_color_f').val();
        $('#target').css('color', b_t_color_f);
        var b_t_size_f = $('#b_t_size_f').val();
        $('#target').css('font-size', b_t_size_f+'px');
        
    }, function() {
        var focused = document.getElementById('target');
        var isFocused = (document.activeElement === focused);
     
        if(isFocused == false) {
            var size_val = $('#size').val();
            $('#target').css('width', size_val+'px');
            $('#target').css('height', size_val+'px');
            $('#target').css('line-height', size_val+'px');
            var back_color = $('#button_color').val();
            $('#target').css('background-color', back_color);
            var border_color = $('#border_color').val();
            var border_size = $('#border_size').val();
            $('#target').css('box-shadow', '0 0 0 '+border_size+'px '+border_color);
            var b_radius = $('#b_radius').val();
            $('#target').css('border-radius', b_radius+'px');
            var b_t_color = $('#b_t_color').val();
            $('#target').css('color', b_t_color);
            var b_t_size = $('#b_t_size').val();
            $('#target').css('font-size', b_t_size+'px');
        }
      
    });
    $("#target").focus(function(){
        
        var size_val_f = $('#size_f').val();
        $('#target').css('width', size_val_f+'px');
        $('#target').css('height', size_val_f+'px');
        $('#target').css('line-height', size_val_f+'px');
        var back_color_f = $('#button_color_f').val();
        $('#target').css('background-color', back_color_f);
        var border_color_f = $('#border_color_f').val();
        var border_size_f = $('#border_size_f').val();
        $('#target').css('box-shadow', '0 0 0 '+border_size_f+'px '+border_color_f);
        var b_radius_f = $('#b_radius_f').val();
        $('#target').css('border-radius', b_radius_f+'px');
        var b_t_color_f = $('#b_t_color_f').val();
        $('#target').css('color', b_t_color_f);
        var b_t_size_f = $('#b_t_size_f').val();
        $('#target').css('font-size', b_t_size_f+'px');
       
    });
  
     $( '#target' ).focusout( function() {
    
         var size_val = $('#size').val();
        $('#target').css('width', size_val+'px');
        $('#target').css('height', size_val+'px');
        $('#target').css('line-height', size_val+'px');
        var back_color = $('#button_color').val();
        $('#target').css('background-color', back_color);
        var border_color = $('#border_color').val();
        var border_size = $('#border_size').val();
        $('#target').css('box-shadow', '0 0 0 '+border_size+'px '+border_color);
        var b_radius = $('#b_radius').val();
        $('#target').css('border-radius', b_radius+'px');
        var b_t_color = $('#b_t_color').val();
        $('#target').css('color', b_t_color);
        var b_t_size = $('#b_t_size').val();
        $('#target').css('font-size', b_t_size+'px');
       
    });
    
    $( '#size_f' ).on('input', function( event ) {
        var size_val_f = $('#size_f').val();
        $('#target').css('width', size_val_f+'px');
        $('#target').css('height', size_val_f+'px');
        $('#target').css('line-height', size_val_f+'px');
        
    });
  
    $( '#button_color_f' ).change(function( event ) {
        var back_color = $(this).val();
        $('#target').css('background-color', back_color);
        
    });
     $( '#b_radius_f' ).on('input', function( event ) {
        var button_radius = $('#b_radius_f').val();
        $('#target').css('border-radius', button_radius+'px');
    });
    
      $( '#b_t_size_f' ).on('input', function( event ) {
        var b_t_size = $('#b_t_size_f').val();
        $('#target').css('font-size', b_t_size+'px');
        
    });
      $("#b_t_color_f").change(function(){
        var color_val = $(this).val();
        $('#target').css('color', color_val);
      });
    $( '#border_color_f' ).change(function( event ) {
      var border_size = $('#border_size_f').val();
      var border_color = $('#border_color_f').val();
      $('#target').css('box-shadow', '0 0 0 '+border_size+'px '+border_color);
    });
    $( '#border_size_f' ).on('input', function( event ) {
      var border_size = $('#border_size_f').val();
      var border_color = $('#border_color_f').val();
      $('#target').css('box-shadow', '0 0 0 '+border_size+'px '+border_color);
    });

    /*focus jquery effects end*/
   
    $('#save_button').on('click', function(){
        var button_html = $("#button_column").html();
        /*focus style*/
        var size_val_f = $('#size_f').val();
        var back_color_f = $('#button_color_f').val();
        var border_color_f = $('#border_color_f').val();
        var border_size_f = $('#border_size_f').val();
        var b_radius_f = $('#b_radius_f').val();
        var b_t_color_f = $('#b_t_color_f').val();
        var b_t_size_f = $('#b_t_size_f').val();
        var focus_style = 'width:'+size_val_f+'px;height:'+size_val_f+'px;line-height:'+size_val_f+'px;border-radius:'+
        b_radius_f+'px;color:'+b_t_color_f+';font-size:'+b_t_size_f+'px;box-shadow:0 0 0 '+
        border_size_f+'px '+border_color_f+';background-color:'+
        back_color_f+';text-decoration:none;text-align: center;padding: 0;margin: 0px;display:block;outline:none';
        var size_val = $('#size').val();
        var back_color = $('#button_color').val();
        var border_color = $('#border_color').val();
        var border_size = $('#border_size').val();
        var b_radius = $('#b_radius').val();
        var b_t_color = $('#b_t_color').val();
        var b_t_size = $('#b_t_size').val();
        var anchor_style = 'width:'+size_val+'px;height:'+size_val+'px;line-height:'+size_val+'px;border-radius:'+
        b_radius+'px;color:'+b_t_color+';font-size:'+b_t_size+'px;box-shadow:0 0 0 '+
        border_size+'px '+border_color+';background-color:'+
        back_color+';text-decoration:none;text-align: center;padding: 0;margin: 0px;display:block;outline:none';
        /*focus end style*/
        
      //  var anchor_style = $("#target").attr('style');
        var button_link = $('#button_link').val();
        var button_text = $('#b_text').val();
        var div_object = new Object();
        div_object.f_size = focus_style;
        div_object.a_style = anchor_style;
        div_object.button_link = button_link;
        div_object.button_text = button_text;

        var button_styles = JSON.stringify(div_object);
      
       $.post(ajaxurl,                        
              {
                action: 'add_button',             
                button_styles: button_styles
               }, function (data) {
                    if(data == 1) {
                        $('#save_b').get(0).click();
                    }else {
                        console.log('something going wrong, Try again!');
                    }
               });
    })
    var Accordion = function(el, multiple) {
        this.el = el || {};
        this.multiple = multiple || false;

        var links = this.el.find('.article-title');
        links.on('click', {
            el: this.el,
            multiple: this.multiple
        }, this.dropdown)
    }

    Accordion.prototype.dropdown = function(e) {
        var $el = e.data.el;
        $this = jQuery(this),
        $next = $this.next();

        $next.slideToggle();
        $this.parent().toggleClass('open');

        if (!e.data.multiple) {
            $el.find('.accordion-content').not($next).slideUp().parent().removeClass('open');
        };
    }
    var accordion = new Accordion(jQuery('.accordion-container'), false);
})( jQuery );
jQuery(document).ready(function () {
    var f_s_value = jQuery("#f_style_value").val();
    var anchor_style_value = jQuery("#anchor_style_value").val();
   
   // if(anchor_style_value.includes('width') == true && anchor_style_value.includes('height') == true) {
        var back_color_d = jQuery("#target").css("background-color");
        var b_size_d = jQuery("#target").css("width");
        var b_font_d = jQuery("#target").css("font-size");
        var b_color_d = jQuery("#target").css("color");
        var b_radius_d = jQuery("#target").css("border-radius");
        var b_shadow_d = jQuery("#target").css("box-shadow");
        var shadow_res = b_shadow_d.split('px');
        if(b_size_d == undefined) {
            var size_res = 0;
        }else {
             var size_res = b_size_d.slice(0,-2);
        }
        var b_font_res = b_font_d.slice(0,-2);
        var b_radius_res = b_radius_d.slice(0,-2);
        var b_shadow_r = shadow_res[3];
        var b_color_res = b_color_d.split('(');
        var b_color_res1 = b_color_res[1].slice(0,-1);
        var b_color_res2 = b_color_res1.split(',');
        var r = b_color_res2[0];
        var g = b_color_res2[1];
        var b = b_color_res2[2];
        function rgbToHex(r, g, b) {
          return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
        }
        var b_color_res3 = rgbToHex(parseInt(r), parseInt(g), parseInt(b)); 

        var back_color_res = back_color_d.split('(');
        var back_color_res1 = back_color_res[1].slice(0,-1);
        var back_color_res2 = back_color_res1.split(',');
        var r = back_color_res2[0];
        var g = back_color_res2[1];
        var b = back_color_res2[2];
        var back_color_res3 = rgbToHex(parseInt(r), parseInt(g), parseInt(b));
        var shadow_border = shadow_res[0].slice(0,-1);
        var shadow_color_res = shadow_border.split('(');
        if(b_shadow_d != 'none') {
            var shadow_color_res1 = shadow_color_res[1].slice(0,-1);
            var shadow_color_res2 = shadow_color_res1.split(',');
            var r = shadow_color_res2[0];
            var g = shadow_color_res2[1];
            var b = shadow_color_res2[2];
            var shadow_color_res3 = rgbToHex(parseInt(r), parseInt(g), parseInt(b));
            jQuery('#border_color').val(shadow_color_res3);
        }
        
        jQuery('#size').val(size_res);
        jQuery('#button_color').val(back_color_res3);
        jQuery('#b_radius').val(b_radius_res); 
        jQuery('#b_t_size').val(b_font_res);
        jQuery('#b_t_color').val(b_color_res3);
        jQuery('input[name="border_size"]').val(parseInt(b_shadow_r));
           
  //  }
   
    
    /*focus hover style*/
  //  if(f_s_value.includes('width') == true && f_s_value.includes('height') == true) {
        var f_back_color_d = jQuery("#f_button").css("background-color");
        var f_b_size_d = jQuery("#f_button").css("width");
        var f_b_font_d = jQuery("#f_button").css("font-size");
        var f_b_color_d = jQuery("#f_button").css("color");
        var f_b_radius_d = jQuery("#f_button").css("border-radius");
        var f_b_shadow_d = jQuery("#f_button").css("box-shadow");
        var f_shadow_res = f_b_shadow_d.split('px');
        var f_size_res = f_b_size_d.slice(0,-2);
        var f_b_font_res = f_b_font_d.slice(0,-2);
        var f_b_radius_res = f_b_radius_d.slice(0,-2);
        var f_b_shadow_r = f_shadow_res[3];
        var f_b_color_res = f_b_color_d.split('(');
        var f_b_color_res1 = f_b_color_res[1].slice(0,-1);
        var f_b_color_res2 = f_b_color_res1.split(',');
        var r = f_b_color_res2[0];
        var g = f_b_color_res2[1];
        var b = f_b_color_res2[2];
       
        function rgbToHex(r, g, b) {
          return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
        }
        var f_b_color_res3 = rgbToHex(parseInt(r), parseInt(g), parseInt(b)); 

        var f_back_color_res = f_back_color_d.split('(');
        var f_back_color_res1 = f_back_color_res[1].slice(0,-1);
        var f_back_color_res2 = f_back_color_res1.split(',');
        var r = f_back_color_res2[0];
        var g = f_back_color_res2[1];
        var b = f_back_color_res2[2];
        var f_back_color_res3 = rgbToHex(parseInt(r), parseInt(g), parseInt(b));
        var f_shadow_border = f_shadow_res[0].slice(0,-1);
        var f_shadow_color_res = f_shadow_border.split('(');
        if(f_b_shadow_d != 'none') {
            var f_shadow_color_res1 = f_shadow_color_res[1].slice(0,-1);
            var f_shadow_color_res2 = f_shadow_color_res1.split(',');
            var r = f_shadow_color_res2[0];
            var g = f_shadow_color_res2[1];
            var b = f_shadow_color_res2[2];
            var f_shadow_color_res3 = rgbToHex(parseInt(r), parseInt(g), parseInt(b));
            jQuery('#border_color_f').val(f_shadow_color_res3);
        }
        jQuery('#size_f').val(f_size_res);
        jQuery('#button_color_f').val(f_back_color_res3);
        jQuery('#b_radius_f').val(f_b_radius_res); 
        jQuery('#b_t_size_f').val(f_b_font_res);
        jQuery('#b_t_color_f').val(f_b_color_res3);
        jQuery('input[name="border_size_f"]').val(parseInt(f_b_shadow_r));
        /*focus hover style end*/
  //  }
});
