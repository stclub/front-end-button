<?php
function create_button() {
	?>
	<div class="button_div">
		<h3><?php  esc_html_e('Create button', 'custom-button'); ?> </h3><hr>
		<div class='row'>
			<?php 
			   
			    if(get_option( 'my_button' )) {
			    	
			    	$b_style = get_option( 'my_button' );
				
					$style_c = str_replace("\\"," ", $b_style);
					$style_array = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $style_c), true );
					$anchor_style = $style_array['a_style '];
					$style_f = $style_array['f_size '];
					if(!isset($style_array['button_link '])) {
						$style_array['button_link '] = '';
					}
					if(!isset($style_array['button_text '])) {
						$style_array['button_text '] = '';
					}
					
					$button_link = $style_array['button_link '];
			    	$button_text = $style_array['button_text '];
			    }else {
			        
			    	$anchor_style = 'text-decoration:none !important;text-align: center;padding: 0;margin: 0px;display:block;outline:none;width: 100px;height: 100px;line-height: 100px;background-color: rgb(0, 128, 0);box-shadow: rgb(221, 221, 221) 0px 0px 0px 15px;border-radius: 100px;color: rgb(0, 0, 0);font-size: 20px;';
			    	$style_f = 'text-decoration:none !important;text-align: center;padding: 0;margin: 0px;display:block;outline:none;width: 100px;height: 100px;line-height: 100px;background-color: #000000;box-shadow: rgb(221, 221, 221) 0px 0px 0px 15px;border-radius: 100px;color: #53718e;font-size: 20px;';
			    	$button_text = 'button';
			    	$button_link = 'https://';

			    }
				
			?>
			<input type="hidden" name="anchor_style_value" id="anchor_style_value" value="<?php echo $anchor_style; ?>">
			<input type="hidden" name="f_style_value" id="f_style_value" value="<?php echo $anchor_style; ?>">

			<div class='column'>
				<h4><?php  esc_html_e('Settings', 'custom-button'); ?> </h4><hr>
				  <!-- accordion start -->
			    <section id="content">
                     
				    <div id="accordion" class="accordion-container">
				        <article class="content-entry">
				            <h5 class="article-title"><i></i><?php  esc_html_e('Button Design', 'custom-button'); ?></h5>
				            <div class="accordion-content">
				            	<br>
				                <div class="form-row">
								    <div class="col">
								    	<label for="Button color"><?php  esc_html_e('Button color', 'custom-button'); ?></label>
								      	<input type="color" id='button_color' name='button_color' value='#008000' class="form-control" style="height:34px">
								    </div>
								    <div class="col">
								    	<label for="Button size"><?php  esc_html_e('Button size', 'custom-button'); ?></label>
								      	<input type="number" id='size' name='button_size' class="form-control" placeholder="Button size" value='100' style="padding:0 0 0 8px">
								    </div>
								</div><br>
								<div class="form-row">
								    <div class="col">
								    	<label for="Border color"><?php  esc_html_e('Border color', 'custom-button'); ?></label>
								      	<input type="color" class="form-control" name='border_color' id='border_color' value='#dddddd' style="height:34px">
								    </div>
								    <div class="col">
								    	<label for="Border size"><?php  esc_html_e('Border size', 'custom-button'); ?></label>
								      	<input type="number" class="form-control" name='border_size' id='border_size'  placeholder="Border size" value='15' style="padding:0 0 0 8px">
								    </div>
								</div><br>
								<div class="form-row">
								    <div class="col">
								    	<label for="Button radius"><?php  esc_html_e('Button radius', 'custom-button'); ?></label>
								      	<input type="number" class="form-control" name='b_radius' id='b_radius' value='100' placeholder="Button radius" style="padding:0 0 0 8px" />
								    </div>
								</div>
							</div>
				            <!--/.accordion-content-->
				        </article>
				    </div>

				</section>
				<section id="content">

				    <div id="accordion" class="accordion-container">
				        <article class="content-entry">
				            <h5 class="article-title"><i></i><?php  esc_html_e('Focus & Hover Effects for Button Design', 'custom-button'); ?></h5>
				            <div class="accordion-content">
				            	<br>
				                <!-- focus html start -->
				                <div class="form-row">
								    <div class="col">
								    	<label for="Button color"><?php  esc_html_e('Button color', 'custom-button'); ?></label>
								      	<input type="color" id='button_color_f' name='button_color_f' value='#000000' class="form-control" style="height:34px">
								    </div>
								    <div class="col">
								    	<label for="Button size"><?php  esc_html_e('Button size', 'custom-button'); ?></label>
								      	<input type="number" id='size_f' value='100' name='button_size_f' class="form-control" placeholder="Button size" style="padding:0 0 0 8px">
								    </div>
								</div><br>
								<div class="form-row">
								    <div class="col">
								    	<label for="Border color"><?php  esc_html_e('Border color', 'custom-button'); ?></label>
								      	<input type="color" class="form-control" name='border_color_f' id='border_color_f' value='#dddddd' style="height:34px">
								    </div>
								    <div class="col">
								    	<label for="Border size"><?php  esc_html_e('Border size', 'custom-button'); ?></label>
								      	<input type="number" class="form-control" name='border_size_f' id='border_size_f' value='15' placeholder="Border size" style="padding:0 0 0 8px">
								    </div>
								</div><br>
								<div class="form-row">
								    <div class="col">
								    	<label for="Button radius"><?php  esc_html_e('Button radius', 'custom-button'); ?></label>
								      	<input type="number" class="form-control" name='b_radius_f' id='b_radius_f' value='100' placeholder="Button radius" style="padding:0 0 0 8px" />
								    </div>
								</div>
				            <!-- focus html end -->
								
				            </div>
				            <!--/.accordion-content-->
				        </article>
				    </div>

				</section>
				<section id="content">

				    <div id="accordion" class="accordion-container">
				        <article class="content-entry">
				            <h5 class="article-title"><i></i><?php  esc_html_e('Text Settings', 'custom-button'); ?></h5>
				            <div class="accordion-content">
				            	<br>
				                <div class="form-row">
								    <div class="col">
								    	<label for="Button text"><?php  esc_html_e('Button text', 'custom-button'); ?></label>
								      	<input type='text' name='b_text' id='b_text' value='<?php echo $button_text;?>' class="form-control" style="padding:0 0 0 8px">
								    </div>
								    <div class="col">
								    	<label for="Text color"><?php  esc_html_e('Text color', 'custom-button'); ?></label>
								      	<input type='color' class="form-control" name='b_t_color' id='b_t_color' value='#000000' class="form-control" style="height:34px">
								    </div>
								</div><br>
								<div class="form-row">
								    <div class="col">
								    	<label for="Text Size"><?php  esc_html_e('Text Size', 'custom-button'); ?></label>
								      	<input type='number' class="form-control" name='b_t_size' id='b_t_size' value='20' placeholder="Text Size" style="padding:0 0 0 8px">
								    </div>
								    <div class="col">
								    	<label for="Button link"><?php  esc_html_e('Button link', 'custom-button'); ?></label>
								      	<input type='text' name='button_link' id='button_link' value='<?php echo $button_link; ?>' class="form-control" style="padding:0 0 0 8px">
								    </div>
								</div>
								
				            </div>
				            <!--/.accordion-content-->
				        </article>
				    </div>

				</section>  
				<section id="content">

				    <div id="accordion" class="accordion-container">
				        <article class="content-entry">
				            <h5 class="article-title"><i></i><?php  esc_html_e('Focus & Hover Effects for Text Settings', 'custom-button'); ?></h5>
				            <div class="accordion-content">
				            	<br>
				                <div class="form-row">
								    
								    <div class="col">
								    	<label for="Text color"><?php  esc_html_e('Text color', 'custom-button'); ?></label>
								      	<input type='color' class="form-control" name='b_t_color_f' id='b_t_color_f' value='#53718e' class="form-control" style="height:34px">
								    </div>
								    <div class="col">
								    	<label for="Text Size"><?php  esc_html_e('Text Size', 'custom-button'); ?></label>
								      	<input type='number' class="form-control" name='b_t_size_f' id='b_t_size_f' value='20' placeholder="Text Size" style="padding:0 0 0 8px">
								    </div>
								</div><br>
				            </div>
				            <!--/.accordion-content-->
				        </article>
				    </div>

				</section>
			    <div  id='save_button' class='button_d'><?php  esc_html_e('Save', 'custom-button'); ?></div>
			    <a href="#s_button" id="save_b"></a>
		        <div id="s_button" class="overlay">
					<div class="popup">
						<h4><?php  esc_html_e('Settings Updated.', 'custom-button'); ?></h4>
						<a class="close" href="#">&times;</a>
					</div>
				</div>
			</div>
			<div class='column'>
				<h4><?php  esc_html_e('Preview', 'button'); ?> </h4><hr>
				<div id='button_column' class="button_center">
					<style>
				    	#target {
				  			<?php echo $anchor_style; ?>
				    	}
				    	
				    
				    </style>
				   
					<a href='<?php echo $button_link; ?>' class='button_style'  id='target' style="">
						<?php  
					    	printf(
					            __( '%s', 'button' ),
					            $button_text
					        ); 
					    ?>
					</a>
					<div class="b_hide" id="f_button" style="<?php echo $style_f; ?>"></div>
					
				</div>
			</div>
		</div>
    </div>
	<?php
}
function get_button_html() {
 
	$b_style = get_option( 'my_button' );
	$style_c = str_replace("\\"," ", $b_style);
	$style_array = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $style_c), true );
	$focus_style = $style_array['f_size '];
	$anchor_style = $style_array['a_style '];
	if(!isset($style_array['button_link '])) {
		$style_array['button_link '] = '';
	}
	if(!isset($style_array['button_text '])) {
		$style_array['button_text '] = '';
	}
    $set_focus_style = str_replace("margin: 0px","right:50px;position:fixed;z-index:999;bottom:100px ", $focus_style);
    $set_a_style = str_replace("margin: 0px","right:50px;position:fixed;z-index:999;bottom:100px ", $anchor_style);
   
    ?>
    <style>
    	.target_b {
  			<?php echo $set_a_style; ?>
    	}
    	.target_b_focus {
  			<?php echo $set_focus_style; ?>
  		}
  	    #target:focus {
  			<?php echo $set_focus_style; ?>
  		}
  		#target:hover {
  			<?php echo $set_focus_style; ?>
  		}
  		#target:active {
  			<?php echo $set_focus_style; ?>
  		}

    </style>
  
    	<a href="<?php echo $style_array['button_link ']; ?>"  id='target' class="target_b" style="text-decoration: none !important">
	    	<?php  
		    	printf(
		            __( '%s', 'button' ),
		            $style_array['button_text ']
		        ); 
		    ?> 
    	</a>
   
	<script>
	
		/*$("#target").hover(function() {
        	$("#target").addClass("target_b_focus");
        	$("#target").removeClass("target_b");
        
    }, function() {
        var focused = document.getElementById('target');
        var isFocused = (document.activeElement === focused);
     
        if(isFocused == false) {
            $("#target").addClass("target_b");
        	$("#target").removeClass("target_b_focus");
        }
      
    });
    $("#target").focus(function(){ 
    	$("#target").addClass("target_b_focus");
        $("#target").removeClass("target_b");
    });
  
    $( '#target' ).focusout( function() {    
        $("#target").addClass("target_b");
        $("#target").removeClass("target_b_focus");
    });*/
	</script>
    <?php
}
add_action( 'wp_footer', 'get_button_html' );