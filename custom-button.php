<?php
/**
 * Plugin Name: Custom Button
 * Description: Custom Button
 * Author: Varion
 * Version: 0.0.1
 * Text Domain: custom-button
 * Domain Path: /languages/
 */
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
    define('BUTTON_BASE_PATH', plugin_dir_url(__FILE__));
    require_once(plugin_dir_path(__FILE__) . 'ajax/ajax.php');
    require_once(plugin_dir_path(__FILE__) . 'page/page.php');
    require_once(plugin_dir_path(__FILE__) . 'scripts/scripts.php');
    require_once(plugin_dir_path(__FILE__) . 'template/create-button.php');

    use CustomButton\Ajax; 
    use CustomButton\Page;  
    use CustomButton\Scripts;  
    use CustomButton\Shortcode;       

    add_action( 'plugins_loaded', 'add_new_button' );
    function add_new_button() {
        load_plugin_textdomain( 'custom-button', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
    	Page::init();
    	Ajax::init();
    	Scripts::init();
    	
    }  