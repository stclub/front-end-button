<?php
namespace CustomButton;
class Page {
	public static function init() {
		add_action('admin_menu', array(__CLASS__, 'add_button_page'));
	}
	public static function add_button_page()
  {
     add_menu_page( 
        __( 'Custom Button', 'custom-button' ),
        'Custom Button',
        'manage_options',
        'Custom Button',
        'create_button'
    );
  }
}
