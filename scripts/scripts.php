<?php
namespace CustomButton;
class Scripts {
	public static function init() {
		add_action('admin_enqueue_scripts', array(__CLASS__, 'add_custom_button'));
	}
	public static function add_custom_button() {
  
	    wp_register_style( 'Custom Button', BUTTON_BASE_PATH . 'assets/css/style.css' );
	    wp_enqueue_style( 'Custom Button' ); 
		wp_register_script( 'custom_button_handler', BUTTON_BASE_PATH . 'assets/js/custom-script.js', ['jquery'], null, true);
	    wp_enqueue_script( 'custom_button_handler' );
    }
}